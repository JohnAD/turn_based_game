# Package

version       = "1.1.2"
author        = "John Dupuy"
description   = "Game rules framework for turn-based games"
license       = "MIT"
srcDir        = "src"

# Dependencies

requires "nim >= 0.18.0"
